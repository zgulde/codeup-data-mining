drop table if exists cohorts;
drop table if exists exercises;
drop table if exists cohort_exercise;

create table cohorts(name, start_date);
create table exercises(name);
create table cohort_exercise (
    cohort_id,
    exercise_id,
    start_date,
    end_date,
    days_since_start
);
