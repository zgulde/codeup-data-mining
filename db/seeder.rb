#!/usr/bin/env ruby

args = ARGF.argv

# will try to read an exercise list from <cohort>.txt

# install the sqlite3 gem
# `gem install sqlite3`

require 'sqlite3'

db = SQLite3::Database.new("dates.db")

db.execute('delete from cohorts')
db.execute('delete from cohort_exercise')
db.execute('delete from exercises')
db.execute('vacuum')

holidays = %w'
  2015-07-02
  2015-07-03
  2015-09-07
  2015-11-11
  2015-11-25
  2015-11-26
  2015-11-27
  2016-03-25
  2016-03-28
  2016-07-01
  2016-07-04
  2016-09-05
' .map {|d| Date.new *d.split('-').map(&:to_i)}

christmas_holidays = Date.new(2015,12,21).upto(Date.new(2016,01,3)).to_a
holidays += christmas_holidays

# get a list of the directories in results
repos = `find ../results/* -type d`.split.map(&:chomp).map {|d| File.expand_path d}
repos.each_with_index do |dir, i|
  puts "\rRepo #{i + 1} of #{repos.length}"
  # go into that directory
  Dir.chdir dir

  # array of hashes with name and exercises property
  #   - exercises is array of hashes with a start and end date and name
  # example of one hash in the array
  # {
  #   name: 'glacier',
  #   start_date: '...',
  #   exercises: [
  #     {start_date: ..., end_date: ..., name: ...},
  #     {start_date: ..., end_date: ..., name: ...}
  #   ]
  # }
  cohorts = [
    {name: 'glacier', start_date: '2015-06-05'},
    {name: 'hampton', start_date: '2015-09-21'},
    {name: 'ike',     start_date: '2016-01-19'},
    {name: 'joshua',  start_date: '2016-03-07'},
    {name: 'kings',   start_date: '2016-05-23'},
    {name: 'lassen',  start_date: '2016-07-18'}
  ].map(&->cohort{
    {
      name: cohort[:name],
      start_date: cohort[:start_date],
      exercises: IO.readlines("#{cohort[:name]}.txt")
                   .map(&:chomp)
                   .map(&:split)
                   .map(&->exercise{ {start_date: exercise[0], end_date: exercise[1], name: exercise[2]} }),
    }
  })

  cohorts.each do |cohort|
    results = db.execute('select oid from cohorts where name = ?', cohort[:name])
    if results.empty?
      db.execute('insert into cohorts(name, start_date) values (?, ?)', cohort[:name], cohort[:start_date])
      cohort[:id] = db.last_insert_row_id
    else
      cohort[:id] = results.first.first
    end

    cohort[:exercises].each do |exercise|

      # check if we already have that exercise in the db, if we do grab it's id,
      # otherwise insert it and grab the inserted id
      results = db.execute('select oid from exercises where name = ?', exercise[:name])
      if results.empty?
        db.execute('insert into exercises(name) values (?)', exercise[:name])
        exercise[:id] = db.last_insert_row_id
      else
        exercise[:id] = results.first.first
      end

      # calculate days since cohort start
      cohort_start_date = Date.new(*cohort[:start_date].split('-').map(&:to_i))
      exercise_start_date = Date.new(*exercise[:start_date].split('-').map(&:to_i))
      days_since_start = cohort_start_date.upto(exercise_start_date)
        .select {|d| ! d.saturday?}
        .select {|d| ! d.sunday?}
        .select {|d| ! holidays.include? d}
        .length

      query = 'insert into cohort_exercise(cohort_id, exercise_id, start_date, end_date, days_since_start)
               values (?, ?, ?, ?, ?)'

      puts "HEY!!!" if cohort[:id].to_i > 6

      db.execute(
        query,
        cohort[:id],
        exercise[:id],
        exercise[:start_date],
        exercise[:end_date],
        days_since_start
      )

    end
  end
end
