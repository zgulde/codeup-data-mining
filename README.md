# Data mining student's github repos

We want to get some data on when cohorts hit certain exercises, this repo
presents a solution to that problem based on mining all the students in that
cohort's github repos for commit dates for various exercises

## How it works

1. We'll start by cloning every student's repo, for example clone every students
`codeup.dev`.

2. Get a list of exercises

    Go through all the repos and get a list of all the files. The list will
    ignore case and allow for differences between file names, that is we'll
    count `hello-world.html` and `Hello_World.html` as the same.

    If more than x number of students share a certain filename, we'll consider
    that an exercise we should look at.

3. Get a start and end date for each exercises

    for every exercise in the previously generated list, we'll go through and
    get a list of the first commit date for that file and the last commit date
    for that file.

    We'll take a look at all the start and end dates and decide on the start and
    end date for the cohort based on the most frequent date.

    For example if we are looking at `hello-world.html`, we'll get a list of the
    dates of the first commit for that file

        2016-01-02
        2016-01-03
        2016-01-02
        2016-01-01
        2016-01-02
        2016-01-03

    and decide that the cohort as a whole started the exercise on `2016-01-02`,
    as that is the most frequent date.

## Usage

1. Clone this repo

1. (Manually) Create a directory and list of the urls for the repo you want to
   look at

    Inside `github-urls` create a directory named after the repo you want to
    look at. This directory will contain text files for each cohort with a list
    of the urls for the students' in that cohorts repo.

    **Example directory structure**

        github-urls/
        ├── codeup.dev
        │   ├── glacier.txt
        │   ├── hampton.txt
        │   ├── ike.txt
        │   ├── joshua.txt
        │   ├── kings.txt
        │   └── lassen.txt
        ├── database-exercises
        │   ├── glacier.txt
        │   ├── hampton.txt
        │   ├── ike.txt
        │   ├── joshua.txt
        │   ├── kings.txt
        │   └── lassen.txt
        ├── php-exercises
        │   ├── glacier.txt
        │   ├── hampton.txt
        │   ├── ike.txt
        │   ├── joshua.txt
        │   ├── kings.txt
        │   └── lassen.txt
        └── simple-simon
            ├── glacier.txt
            ├── hampton.txt
            ├── ike.txt
            ├── joshua.txt
            ├── kings.txt
            └── lassen.txt

    For example, if we want to look at the exercises in student's `codeup.dev`
    repos, we'll have to go look at every student in that cohort's github and
    determine which repo contains those exercises. We'll put those urls in a
    file named `cohort.txt` where `cohort` is the name of the cohort, and where
    the file contains each url on a new line.

    This part was hard to automate as we have to account for differences between
    repos named `codeup.dev`, `codeup-web-exercises`, and `codeup-exercises`. In
    theory this could be automated by having a list of github usernames and
    hitting the github api to determine commit dates.

    We will have to repeat this entire process for different repos, i.e. we will
    have to go through this process separately for `codeup.dev` exercises and
    the separate `php-exercises` repos.

    The file you create should look like:

        https://github.com/someStudent/codeup.dev
        https://github.com/anotherstudent/Codeup-Web-Exercises
        https://github.com/someStudent/codeup-exercises
        https://github.com/someStudent/codeup_WEB_exercises
        ...

1. Run `./get-all` which will go through all the directories in `github-urls`
   and process all the data and spit out the results to `results/` the directory
   structure for results will be the same as it was for `githu-urls/`, but the
   text files will contain start and end dates for each exercise 

    Example output `results/codeup.dev/cohort.txt`

        2016-01-01 2016-01-02 calculator.js
        2016-01-10 2016-01-12 weather_map.js
        ...




