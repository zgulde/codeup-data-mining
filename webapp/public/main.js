'use strict';

let app = new Vue({
    el: '#app',
    data: {
        exercises: [],
        exerciseSearch: '',
        cohortSearch: '',
        minNumberCohorts: 1,
        daysIntoCohort: 0,
        daysFuzz: 3
    },
    filters: {
        'searchFilter': function (exercises) {
            let minNumberCohorts = this.minNumberCohorts;
            let daysIntoCohort = parseInt(this.daysIntoCohort);
            let daysFuzz = parseInt(this.daysFuzz);
            let cohortSearch = this.cohortSearch;
            let exerciseSearch = this.exerciseSearch;
            // let exs = _
            let exs = _
                .filter(exercises, e => e.cohorts.length >= minNumberCohorts)
                .filter(e => daysIntoCohort <= 0 || e.avg_days >= daysIntoCohort - daysFuzz)
                .filter(e => daysIntoCohort <= 0 || e.avg_days <= daysIntoCohort + daysFuzz)
                .filter(e => e.cohorts.some(c => c.name.includes(cohortSearch)))
                .filter(e => e.name.includes(exerciseSearch));

            $('#total').text(exs.length);
            return exs;
        },
        'length': a => { a.length }
    }
})

$.get('/all').done(data => {
    let exercises = data.map(exercise => {
        exercise.avg_days = exercise.cohorts .reduce((sum, c) => sum + c.days_since_start, 0) / exercise.cohorts.length
        return exercise;
    }).sort((e1, e2) => e1.avg_days - e2.avg_days);
    app.exercises = exercises;
});

