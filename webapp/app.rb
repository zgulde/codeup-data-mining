require 'date'
require 'sinatra'
require 'sqlite3'
require 'json'

$db = SQLite3::Database.new("dates.db")

def exercises
  $db.execute('select oid, name from exercises')
     .map { |e| {id: e[0], name: e[1]} }
end

def cohorts_for_exercise e_id
  query = 'select name, ce.days_since_start, ce.start_date
           from cohort_exercise as ce
           join cohorts as c on c.oid = ce.cohort_id
           where ce.exercise_id = ?'
  $db.execute(query, e_id).map do |row|
    {name: row[0], days_since_start: row[1], start_date: row[2]}
  end
end

get '/' do
  IO.read('./public/index.html')
end

get '/all' do 
  content_type :json
  exercises_with_cohorts = exercises.map do |exercise|
    cohorts = cohorts_for_exercise exercise[:id]
    exercise.merge(cohorts: cohorts)
  end
  JSON.generate exercises_with_cohorts
end
